# About
This is intended as an image "cache" that serves two purposes.
- Store and serve all the legacy images from the web archive
- Also consider duplicate images so not to waste bandwidth on serving the same image twice.
- Give a possibility to serve used images once they go down

This could be extended to an image library, but it has never been done since image hosting and bandwidth is expensive AF.

## Needed to run
- A configured image folder in the config.js
    - I recommend keeping this path separate