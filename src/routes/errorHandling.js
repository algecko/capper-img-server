const route = app => {
// 404 route
	app.use((req, res) => {
		res.status(404).end()
	})

	// noinspection JSUnusedLocalSymbols
	app.use((err, req, res, next) => {
		const {extMessage, message, statusCode = 500} = err

		const logMessage = message || extMessage || err
		if (logMessage && process.env.NODE_ENV !== 'test')
			console.error(logMessage)

		const myResponse = {}
		if (extMessage)
			myResponse.message = extMessage

		res.status(statusCode).json(myResponse)
	})
}

module.exports = route