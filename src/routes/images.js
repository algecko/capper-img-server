const express = require('express')
const mime = require('mime')
const path = require('path')
const fs = require('fs')

const decodeId = require('../tools/decodeId')
const dedup = require('../tools/deduplication')
const imgCache = require('../tools/imgCache')

const router = express.Router()

router.use('/:id', (req, res) => {
	const seq = req.query.seq
	const imgId = '' + decodeId(req.params.id) + (typeof (seq) === 'undefined' ? '' : `_seq_${seq}`)

	const mappedDupl = dedup(imgId)
	if (mappedDupl)
		return res.redirect(`/images/${mappedDupl.to}`)

	const file = imgCache.get(imgId)
	if (!file)
		return req.query.url ? res.redirect(req.query.url) : res.status(404).end('Not found')

	const type = mime.getType(path.extname(file))
	const s = fs.createReadStream(file)
	s.on('open', function () {
		res.set('Content-Type', type)
		s.pipe(res)
	})
	s.on('error', function () {
		res.set('Content-Type', 'text/plain')
		res.status(404).end('Not found')
	})

})


module.exports = router