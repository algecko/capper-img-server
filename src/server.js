const express = require('express')
const cors = require('cors')

const errorHandling = require('./routes/errorHandling')

const app = express()

app.use(cors({
	origin: process.env.NODE_ENV === 'production' ? /(https?:\/\/opentgc\.com|https?:\/\/95\.216\.148\.182)/ : '*'
}))

app.use('/images', require('./routes/images'))

app.use((req, res) => {
	res.status(404).send('Page not Found')
})

errorHandling(app)

module.exports = app