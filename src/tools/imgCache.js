const NodeCache = require('node-cache')
const path = require('path')
const chokidar = require('chokidar')

const config = require('../../config.json')

const imgCache = new NodeCache({checkperiod: 0})

const getKey = filePath => path.basename(filePath, path.extname(filePath))
const getValue = filePath => path.join(config.imgFolder, path.basename(filePath))


chokidar.watch(config.imgFolder, {
	interval: config.fsPollingInterval || 100,
	ignorePermissionErrors: true
})
	.on('add', (changedPath) => {
		imgCache.set(getKey(changedPath), getValue(changedPath))
	})
	.on('unlink', (changedPath) => {
		imgCache.del(getKey(changedPath))
	})

module.exports = imgCache