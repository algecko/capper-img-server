const isEncoded = (string) => Buffer.from(string, 'base64').toString().toLowerCase().startsWith('post')

module.exports = (id) => {
	if (!isEncoded(id))
		return id

	const decoded = Buffer.from(id, 'base64').toString()
	const parts = decoded.split(':')
	if (parts.length !== 2)
		return id

	return parts[1]
}