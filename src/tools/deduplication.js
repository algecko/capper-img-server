const path = require('path')
const chokidar = require('chokidar')
const fs = require('fs')

const config = require('../../config')

const absPath = path.join(__dirname, '../../dupl-mapping.json')

let duplMapping

const refreshDuplMapping = () => {
	try {
		duplMapping = JSON.parse(fs.readFileSync(absPath))
	} catch (e) {
		// if mapping isn't set already initialize it with an empty array, otherwise do nothing
		if (!duplMapping)
			duplMapping = []
	}
}

refreshDuplMapping()

chokidar.watch(absPath, {
	interval: config.fsPollingInterval || 100,
	ignoreInitial: true
})
	.on('change', () => {
		refreshDuplMapping()
	})

module.exports = (sourceID) => duplMapping.find(dm => dm.from === sourceID)
